package com.example.admin.nbreal.etc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class SocketThreadClass {
    Context context;
    private InetSocketAddress socketAddress;
    private SocketChecker socketCheckerTh;

    public Socket openSocket(Socket socket, Context context){
        MakeSocket makeSocket = new MakeSocket(socket);
        makeSocket.start();
        this.context=context;
        return makeSocket.socket;
    }

    public void sendData(Socket socket,String cls, String method, String data){
        SendData sender = new SendData(socket,cls,method,data);
        sender.start();
    }

    public int socketChecker(Socket socket) {
        if(socketCheckerTh==null) {
            socketCheckerTh = new SocketChecker(socket);
            socketCheckerTh.start();
        }

        return socketCheckerTh.re;
    }

    public class MakeSocket extends Thread{// 소켓 열기
        Socket socket;

        MakeSocket(Socket socket){
            this.socket=socket;
        }

        public void run(){
            try {

                socket = new Socket(); // 소켓 연결 반응 확인하기

                socketAddress= new InetSocketAddress("192.168.219.238",9999);
                socket.connect(socketAddress,5000);


            } catch (IOException e) {
                e.printStackTrace();
                // 소켓 연결 실패 관련 처리
            }
        }
    }
    public class SendData extends Thread{ // 소켓 통해 발신
        Socket socket;
        String data;
        int dataLength;

        public SendData(Socket socket, String cls, String method, String data){
            this.socket=socket;
            this.data=data;
            dataLength = data.length();

            byte[] length = new byte[4];
            length =intToByteArray(dataLength); // int 데이터를 바이트에 담기
            byte[] byteData = new byte[dataLength+4];
            System.arraycopy(dataLength, 0, byteData, 0, 4);
            System.arraycopy(data.getBytes(), 0, byteData, 4, data.getBytes().length);

            try {
                DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }




        }

        public void run(){
            /*
            매개변수 소켓을 저장
            매개변수 문자열을 저장
            문자열 인코딩하기
            문자열의 길이 따기

            */

        }
    }

    public class GetData extends Thread{
        Socket socket;
        String data;
        public GetData(Socket socket){

        }
    }
    protected static byte[] intToByteArray(final int integer) {
        ByteBuffer buff = ByteBuffer.allocate(Integer.SIZE / 8);
        buff.putInt(integer);
        buff.order(ByteOrder.BIG_ENDIAN);
        //buff.order(ByteOrder.LITTLE_ENDIAN);
        return buff.array();
    }

    public class SocketChecker extends Thread{
        Socket socket;
        boolean ch;
        int re;

        public SocketChecker(Socket socket){
            this.socket=socket;


        }

        public void run(){
            for(int i=0; i<25; i++){
                re=0;
                try{
                    Thread.sleep(200);
                     ch=  socket.isConnected();
                }catch (NullPointerException e ){

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(ch){
                    i=26;
                re=1;//성공
                }
                else {
                re=2;//실패
                }

            }
        }
    }

}
