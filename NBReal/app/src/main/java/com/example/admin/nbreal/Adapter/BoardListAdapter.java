package com.example.admin.nbreal.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.admin.nbreal.R;
import java.util.ArrayList;

public class BoardListAdapter extends RecyclerView.Adapter< BoardListAdapter.ViewHolder >  {

    private Context context;
    private ArrayList<String> list;
    private String data;

    public BoardListAdapter(Context con, ArrayList<String> list){
        context=con;
        this.list=list;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView userId,itemTitle,data1,data2;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            userId=itemView.findViewById(R.id.id_uploader);
            itemTitle=itemView.findViewById(R.id.title_item);
            data1 =itemView.findViewById(R.id.data1_item);
            data2 =itemView.findViewById(R.id.data2_item);
        }

        public void setItemData(String data,int i) {
            userId.setText(data);
            data1.setText(String.valueOf(i));
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_borad_list,viewGroup,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        data = list.get(i);
        viewHolder.setItemData(data,i);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
