package com.example.admin.nbreal.View;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.nbreal.R;

public class LoginActivity extends AppCompatActivity {

    private EditText editUserId;
    private TextView textAccept;
    private String userId;
    SharedPreferences sP;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sP = getSharedPreferences("user_data",MODE_PRIVATE);
        editor = sP.edit();
        editor.remove("user_id");
        editor.commit();

        setListener();
    }

    private void setListener() {
        editUserId= findViewById(R.id.login_edit_id);
        textAccept= findViewById(R.id.login_btn_accept);

        textAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userId = editUserId.getText().toString();
                // id 유효성 검사 , 중복 검사 필요
                if(userId.length()!=0){

                    editor.putString("user_id",userId);
                    editor.commit();
                    Intent toBoard = new Intent(LoginActivity.this, BoardActivity.class);
                    startActivity( toBoard);

                }else{
                    Toast.makeText(LoginActivity.this, "올바르지 않은 id", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
