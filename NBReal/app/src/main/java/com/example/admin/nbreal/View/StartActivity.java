package com.example.admin.nbreal.View;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.admin.nbreal.R;
import com.example.admin.nbreal.Service.NetworkService;

public class StartActivity extends AppCompatActivity {

    boolean userDataSaved;
    private String userId;
    final static int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 0;
    private NetworkService ns;
    private boolean isService;
    private boolean r=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        Intent serviceStart = new Intent(getApplicationContext(), NetworkService.class);
        startService(serviceStart); // 소켓 생성, 발신/수신 스레드 생성



    }



    @Override
    protected void onStart() {
        super.onStart();

        checkPer();

        SharedPreferences sP = getSharedPreferences("user_data",MODE_PRIVATE); // user_data 라는 파일명으로 데이터 저장하기
        userId = sP.getString("user_id",""); // user_data 라는 파일에 저장된 데이터를 sP에 저장, user_id 키값으로 검색


        if(userId.length()==0){ // 검색 결과 데이터의 길이가 0임, 저장된 데이터 없음.
            userDataSaved=false;
        }else {                     // 저장된 데이터 있음.
            userDataSaved=true;
        }

        if(userDataSaved){
            Intent goBoard = new Intent(this, BoardActivity.class);
            startActivity(goBoard);
        }
        else{
            Intent goLogin = new Intent(this, LoginActivity.class);
            startActivity(goLogin);
        }
    }

    private void checkPer() {
        if (ContextCompat.checkSelfPermission(this,Manifest.permission.READ_CONTACTS)!= PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                                                            new String[]{Manifest.permission.READ_CONTACTS},
                                                            MY_PERMISSIONS_REQUEST_READ_CONTACTS);


        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS:

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // 권한 허가
// 해당 권한을 사용해서 작업을 진행할 수 있습니다
                } else {
                    // 권한 거부
                    finish();
                }
                return;
        }
    }

    ServiceConnection conn = new ServiceConnection() { // 서비스 연결
        public void onServiceConnected(ComponentName name,
                                       IBinder service) {
// 서비스와 연결되었을 때 호출되는 메서드
// 서비스 객체를 전역변수로 저장
            NetworkService.MyBinder mb = (NetworkService.MyBinder) service;
            ns = mb.getService(); // 서비스가 제공하는 메소드 호출하여
// 서비스쪽 객체를 전달받을수 있슴
            isService = true;
        }
        public void onServiceDisconnected(ComponentName name) {
// 서비스와 연결이 끊겼을 때 호출되는 메서드
            isService = false;
        }
    };
}
