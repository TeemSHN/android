package com.example.admin.nbreal.Service;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.Trace;
import android.widget.Toast;

import com.example.admin.nbreal.Adapter.BoardListAdapter;
import com.example.admin.nbreal.etc.NetworkSreviceMethod;
import com.example.admin.nbreal.etc.SocketThreadClass;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

public class NetworkService extends Service {
    //서비스 시작과 동시에 저장된 계정 확인하기
    //서비스 시작 직후 서버와 연결하기 및 로그인
    //소켓 데이터 수신 스레드 반복시키기
    public SocketThreadClass STC;
    public static Socket socket;
    IBinder mBinder = new MyBinder();
    ArrayList<String> boardList;

    NetworkSreviceMethod nsm = new NetworkSreviceMethod(socket);



    public class MyBinder extends Binder {
        public NetworkService getService() { // 서비스 객체를 리턴
            return NetworkService.this;
        }
    }

    public void showToast(Context con,String data){
        Toast.makeText(con, data, Toast.LENGTH_SHORT).show();
    }

    public ArrayList<String>  getBoardList(){
        boardList.add("first");
        boardList.add("2nd");
        boardList.add("3rd");
        return boardList;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        // 서비스 호출 최초 생성되며 실행.
        STC = new SocketThreadClass();
        socket = STC.openSocket(socket,getApplicationContext()); // 소켓 열기
        boardList = new ArrayList();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // 서비스가 호출될 때마다 실행
        SocketReceicer socketReceicer = new SocketReceicer();
        socketReceicer.start();


        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        socket = null;
        // 서비스 종료

    }



    public class SocketReceicer extends Thread{

        int re;
        public void run(){


            while(re==0){
                try {
                    Thread.sleep(200);
                    re = STC.socketChecker(socket);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            switch (re){
                case 1:
                    Intent i = new Intent();
                    i.setAction("com.example.admin.nbreal.BoradCastReceiver.NoticeServiceStart");
                    sendBroadcast(i);
                    break;
                case 2:
                    //sendBroadcast(new Intent("com.example.admin.nbreal.BoradCastReceiver.NoticeConnectFail"));

                    Intent j = new Intent();
                    j.setAction("com.example.admin.nbreal.BoradCastReceiver.NoticeConnectFail");
                    sendBroadcast(j);
                    break; // 연결 실패 브로드캐스트

            }
        }



    }

}