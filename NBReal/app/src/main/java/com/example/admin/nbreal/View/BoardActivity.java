package com.example.admin.nbreal.View;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.nbreal.Adapter.BoardListAdapter;
import com.example.admin.nbreal.R;
import com.example.admin.nbreal.Service.NetworkService;
import com.example.admin.nbreal.Service.NetworkService.MyBinder;

import java.util.ArrayList;

public class BoardActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private NetworkService ns;
    private boolean isService;

    ArrayList<String> boardList = new ArrayList<String>();
    TextView boardText;

    BroadcastReceiver brcReceiver;
    private BoardListAdapter adapter;
    private String BROADCAST_MESSAGE_FAIL="com.example.admin.nbreal.BoradCastReceiver.NoticeConnectFail";
    private String BROADCAST_MESSAGE_SUCCESS="com.example.admin.nbreal.BoradCastReceiver.NoticeServiceStart";
    private IntentFilter theFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board);
        setToolBar();
        setBroadCastRCV();
        serviceStarter();
        setBtnLitsener();
        setBoardList();

    }

    private void setBroadCastRCV() {

        if(brcReceiver!=null){return;}

        theFilter = new IntentFilter();
        theFilter.addAction(BROADCAST_MESSAGE_FAIL);
        theFilter.addAction(BROADCAST_MESSAGE_SUCCESS);


        brcReceiver = new BroadcastReceiver(){
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(BROADCAST_MESSAGE_FAIL)){
                    Toast.makeText(BoardActivity.this, "서버 연결 실패", Toast.LENGTH_SHORT).show();
                }else if(intent.getAction().equals(BROADCAST_MESSAGE_SUCCESS)){
                    boardList = ns.getBoardList();
                    Toast.makeText(BoardActivity.this, "서버 연결 성공", Toast.LENGTH_SHORT).show();
                    isService = true;
                }


            }

        };

    }

    @Override
    protected void onStart() {

        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(brcReceiver,theFilter);
        adapter.notifyDataSetChanged();
    }

    private void setBoardList() {
        recyclerView = findViewById(R.id.board_list_main);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutFrozen(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new BoardListAdapter(this,boardList);
        recyclerView.setAdapter(adapter);
    }

    private void setBtnLitsener() {
        boardText = findViewById(R.id.board_text_main);
        boardText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isService) {
                    ns.showToast(BoardActivity.this,"서비스 연결 해제, 서비스 종료.");
                    serviceStopper();
                    isService=false;
                }else{
                    serviceStarter();
                    Toast.makeText(BoardActivity.this, "서비스 연결", Toast.LENGTH_SHORT).show();
                    isService=true;
                }
            }
        });
    }

    private void setToolBar() {
        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.showOverflowMenu();
    }


    private void serviceStarter() {
        Intent startSer = new Intent(this, NetworkService.class);
        bindService(startSer,conn,Context.BIND_AUTO_CREATE);
    }

    private void serviceStopper() {
        ns.showToast(this,"서비스 메서드를 통한 토스트알림:서비스 해제");
        Intent stopSer = new Intent(this, NetworkService.class);
        stopService(stopSer);
    }

    ServiceConnection conn = new ServiceConnection() { // 서비스 연결
        public void onServiceConnected(ComponentName name,
                                       IBinder service) {
// 서비스와 연결되었을 때 호출되는 메서드
// 서비스 객체를 전역변수로 저장
            MyBinder mb = (MyBinder) service;
            ns = mb.getService(); // 서비스가 제공하는 메소드 호출하여
// 서비스쪽 객체를 전달받을수 있슴
            isService = true;
        }
        public void onServiceDisconnected(ComponentName name) {
// 서비스와 연결이 끊겼을 때 호출되는 메서드
            isService = false;
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                // User chose the "Settings" item, show the app settings UI...
                return true;
            case R.id.action_favorite:
                // User chose the "Favorite" action, mark the current item
                // as a favorite...
                return true;
            case R.id.action_logout:
               Intent i = new Intent(this,LoginActivity.class);
               startActivity(i);
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.board_menu,menu);
        return true;
    }


}
